<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(string $string, $mail)
 * @method static create(array $array)
 */
class EmailVerificationToken extends Model
{
    use HasFactory;
    protected $guarded = [];
}
