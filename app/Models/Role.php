<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Permission\Models\Role as BaseRole;

/**
 * @method static paginate(int $int)
 */
class Role extends BaseRole
{
    use HasFactory;

    public const ADMIN = 'Admin';
    public const USER = 'User';

    /*
     * @return array<string>
     */
    public function allRole(): array
    {
        return [
            self::ADMIN,
            self::USER
        ];
    }
}
