<?php

namespace App\Http\Controllers\Api\Auth;

use App\Customs\Services\EmailVerificationService;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegistrationRequest;
use App\Http\Requests\ResendVerifyEmailRequest;
use App\Http\Requests\VerifyEmailRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;

class AuthController extends Controller
{
    /*
     *
     * this method use to handle email verification
     *
     */
    public function __construct(private EmailVerificationService $service)
    {
    }

    /*
     *
     * this method use to handle verify user email
     *
     */
    public function verifyUserEmail(VerifyEmailRequest $verifyEmailRequest): ?JsonResponse
    {
        return $this->service->verifyEmail($verifyEmailRequest->email, $verifyEmailRequest->token);
    }

    /*
     *
     * this method use to handle resend verification link
     *
     */
    public function resendEmailVerificationLink(ResendVerifyEmailRequest $resendEmailVerificationRequest)
    {
        return $this->service->resendLink($resendEmailVerificationRequest->email);
    }

    /*
     *
     * this method use to handle login operation
     *
     */
    public function login(LoginRequest $loginRequest): JsonResponse
    {
        $token = auth()->attempt($loginRequest->validated());
        if ($token) {
            return $this->responseWithToken($token, auth()->user());
        } else {
            return response()->json([
                'message' => 'An error occur while try to login!'
            ], 401);
        }
    }

    /*
     *
     * this method use to handle register operation
     *
     */
    public function register(RegistrationRequest $registrationRequest): JsonResponse
    {
        $user = User::create($registrationRequest->validated());
        if ($user) {
            $this->service->sendVerificationLink($user);
            $token = auth()->login($user);
            return $this->responseWithToken($token, $user);
        } else {
            return response()->json([
                'message' => 'An error occur while try to create user!'
            ], 500);
        }
    }

    /*
     *
     * this method use to handle jwt access token
     *
     */
    public function responseWithToken($token, $user): JsonResponse
    {
        return response()->json([
            'message' => 'success',
            'user' => $user,
            'access_token' => $token,
            'type' => 'bearer'
        ], 200);
    }
}
