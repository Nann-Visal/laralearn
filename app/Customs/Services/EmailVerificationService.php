<?php

namespace App\Customs\Services;

use App\Models\EmailVerificationToken;
use App\Models\User;
use App\Notifications\EmailVerificationNotification;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

class EmailVerificationService
{
    /*
     *
     * this method handle operation sending the link after generate to a user
     *
     */
    public function sendVerificationLink($user): void
    {
        Notification::send($user, new EmailVerificationNotification($this->linkEmailVerificationGenerator($user->email)));
    }

    /*
     *
     * this method use to handle resend link operation
     *
     */
    public function resendLink($email)
    {
        $user = User::where('email', $email)->first();
        if ($user) {
            $this->sendVerificationLink($user);
            response()->json([
                'status' => 'success',
                'message' => 'Email verification send successfully!'
            ]);
        } else {
            response()->json([
                'status' => 'failed',
                'message' => 'User not found!'
            ]);
        }
    }

    /*
     *
     * this method use to handle if user has been verified already
     *
     */
    public function checkIfEmailVerified($user): void
    {
        if ($user->email_verified_at != null) {
            response()->json([
                'status' => 'failed',
                'message' => 'Email has been already verified!'
            ])->send();
            exit;
        }
    }

    /*
     *
     * this method use to handle verify user email
     *
    */
    public function verifyEmail(string $email, string $token)
    {
        $user = User::where('email', $email)->first();
        if (!$user) {
            response()->json([
                'status' => "failed",
                'message' => 'User not found!'
            ])->send();
            exit;
        }
        $this->checkIfEmailVerified($user);
        $verifiedToken = $this->verifyToken($email, $token);
        if ($user->markEmailAsVerified()) {
            $verifiedToken->delete();
            return response()->json([
                'status' => 'Success',
                'message' => 'Email has been verified successfully!'
            ]);
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'Email verification failed, please try again later'
            ]);
        }
    }

    /*
     *
     * this method handle verify token
     *
     */
    public function verifyToken(string $email, string $token)
    {
        $token = EmailVerificationToken::where('email', $email)->where('token', $token)->first();
        if ($token) {
            if ($token->expired_at >= now()) {
                return $token;
            } else {
                $token->delete();
                response()->json([
                    'status' => 'failed',
                    'message' => 'Token expired!'
                ])->send();
                exit;
            }
        } else {
            response()->json([
                'status' => 'failed',
                'message' => 'Invalid token'
            ])->send();
            exit;
        }
    }

    /*
     *
     * this method use handle  generate link verification
     *
     */
    public function linkEmailVerificationGenerator(string $email): string
    {
        $checkIfTokenExists = EmailVerificationToken::where('email', $email)->first();
        if ($checkIfTokenExists) $checkIfTokenExists->delete();
        $token = Str::uuid();
        $url = config('app.url') . "?token=" . $token . "&email=" . $email;
        $saveToken = EmailVerificationToken::create([
            'email' => $email,
            'token' => $token,
            'expired_at' => now()->addMinutes(60),
        ]);
        if ($saveToken) {
            return $url;
        }
    }
}
